#pragma region headers
//Basic Necessities
#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,sse4.1,sse4.2,popcnt,abm,mmx,avx,avx2,fma,tune=native")
#include<iostream>
#include<cassert>
#include<algorithm>
#include<numeric>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<cstdio>
#include<thread>
#include<ctime>
#include<cctype>
#include<bitset>
#include<fstream>
#include<tuple>
//C++ templates
#include<queue>
#include<deque>
#include<stack>
#include<vector>
#include<string>
#include<set>
#include<unordered_set>
#include<map>
#include<unordered_map>
#pragma endregion
#pragma region shortcuts
//Shortcuts
#define MAX (int64_t)1e18+7
#define MIN (int64_t)-1e18-7
#define N (int64_t)1e5+7
#define M (int64_t)1e6+7
#define mid (l+r)/2
#define outl(a) printf("%lld\n",a)
#define outs(a) printf("%lld ",a)
#define out(a)  printf("%lld",a)
#define mod 1000000007
#define mem(a) memset(a,0,sizeof(a))
#define all(a) a.begin(),a.end()
#define mp(a,b) make_pair(a,b)
using namespace std;
int64_t scan(){
    int64_t a;
    scanf("%lld",&a);
    return a;
}
#pragma endregion
#pragma region globalVariables
    int64_t X[]={0,1,0,-1};
    int64_t Y[]={1,0,-1,0};
    double diff;
    double num[2][1007][1007];
    int64_t bit=0;
    int64_t n;
    double threshold;
    string filename;
    void clearVariables(){
    
    }
#pragma endregion
#pragma region functions
    #include <chrono>
    #include<random>
    vector<double> random_number_generator(int64_t n)
    {
        double lbound = 0;
        double ubound = 10;
        uniform_real_distribution<double> urd(lbound, ubound);
        default_random_engine re;
        vector<double> v;
        for (int i = 0; i < n; i++){
            v.push_back(urd(re));
        }
        return v;
    }   
    void doIt(int64_t s,int64_t e){
        int64_t lim=min(e,n*n);
        for(s;s<lim;s++){
            int64_t a=s/n,b=s%n;
            vector<double> temp;
            temp.push_back(num[bit][a][b]);
            for(int64_t x:X){
                for(int64_t y:Y){
                    if(a+x<n&&a+x>=0&&b+y<n&&b+y>=0){
                        temp.push_back(num[bit][a+x][b+y]);
                    }
                }
            }
            double sum=0.0;
            for(double i:temp){
                sum+=i;
            }
            double_t len=(double)temp.size();
            num[1-bit][a][b]=sum/len;
            diff=max(diff,abs(num[1-bit][a][b]-num[bit][a][b]));
        }
    }
#pragma endregion
int64_t sum_time=0;
// int main(int argc,char **argv)
void solve(int64_t a,double b,int64_t step)
{
    // assert(argc==3);
    n=a;// n=stoi(argv[1]);
    threshold=b;// threshold=stof(argv[2]);

    auto v=random_number_generator(n*n);
    for(int64_t i=0;i<n;i++){
        for(int64_t j=0;j<n;j++){
            num[0][i][j]=v[i*n+j];
        }
    }
    auto start = chrono::high_resolution_clock::now();
    diff=threshold+1.0;
    int64_t iter=0;
    int64_t count_threads=0;
    while(diff>=threshold){
        diff=0;
        vector<thread> u;
        for(int64_t i=0;i<n*n;i+=step){
            u.push_back(thread(doIt,i,i+step));
        }
        for(auto &i:u){
            count_threads++;
            i.join();
        }
        u.clear();
        bit=1-bit;
        iter++;
    }
    bit=1-bit;
    auto stop = chrono::high_resolution_clock::now();
    ofstream fout;
    fout.open(filename,ofstream::app);
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    fout<<n<<" "<<duration.count()<<" "<<iter<<" "<<count_threads<<endl;
    sum_time+=duration.count();
    fout.close();
}
int main(int argc,char **argv){
    int64_t multiple=stoi(argv[1]);
    filename=string(argv[2]);
    for(int64_t i=1;i<=500;i++){
        solve(i,0.01,multiple*i);
    }
    cout<<sum_time<<endl;
}
#include <thrust/host_vector.h>
#include <thrust/generate.h>
#include <thrust/functional.h>
#include<iostream>
#include<vector>
using namespace std;
int main(void)
{
          vector<int> v;
          int n;
          cin>>n;
          for(int i=0;i<n;i++){
                    int a;
                    cin>>a;
                    v.push_back(a);
          }
          thrust::host_vector<int> h_vec(v.begin(),v.end());
          thrust::sort(h_vec.begin(), h_vec.end());
          thrust::copy(h_vec.begin(), h_vec.end(), v.begin());
          for(int i:v){
                    cout<<i<<" ";
          }
          cout<<endl;
          return 0;
}

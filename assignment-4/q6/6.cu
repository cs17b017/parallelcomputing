#include<cuda_runtime.h>
#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
#define MAX 100007
__global__ void part2(int *dev_visited,int *dev_cost,int *dev_minimalCost,int n){
          int id=threadIdx.x;
          if(id>n){
                    return;
          }
          if(dev_cost[id]>dev_minimalCost[id]){
                    dev_cost[id]=dev_minimalCost[id];
                    dev_visited[id]=1;
          }
          dev_minimalCost[id]=dev_cost[id];
}
__global__ void part1(int *dev_edges,int *dev_visited,int *dev_cost,int *dev_minimalCost,int n){
          int id=threadIdx.x;
          if(id>n){
                    return;
          }
          if(dev_visited[id]){
                    dev_visited[id]=0;
                    for(int i=107*id;i<107*id+107;i++){
                              if(dev_edges[i]!=MAX && dev_minimalCost[i-107*id]>dev_cost[id]+dev_edges[i]){
                                        dev_minimalCost[i-107*id]=dev_cost[id]+dev_edges[i];  
                              }
                    }
          }
}
int main(){
          int n,m;
          cin>>n>>m;
          int *edges=(int*)malloc(sizeof(int)*107*107);
          for(int i=0;i<107*107;i++){
                    edges[i]=MAX;
                    if(i*107<107*107){
                              edges[i*107]=0;
                    }
          }
          for(int i=0;i<m;i++){
                    int a,b,c;
                    cin>>a>>b>>c;
                    edges[107*a+b]=c;
                    edges[107*b+a]=c;
                    
          }
          int visited[107];
          for(int i=0;i<107;i++){
                    visited[i]=0;
          }
          int cost[107];
          for(int i=0;i<107;i++){
                    cost[i]=MAX;
          }
          int minimalCost[107];
          for(int i=0;i<107;i++){
                    minimalCost[i]=MAX;
          }
          int *dev_edges;
          cudaMalloc((void**)&dev_edges,sizeof(int)*107*107);
          cudaMemcpy(dev_edges,edges,sizeof(int)*107*107,cudaMemcpyHostToDevice);
          int *dev_visited;
          cudaMalloc((void**)&dev_visited,sizeof(int)*107);
          int *dev_cost;
          cudaMalloc((void**)&dev_cost,sizeof(int)*107);
          int *dev_minimalCost;
          cudaMalloc((void**)&dev_minimalCost,sizeof(int)*107);
          int start;
          cin>>start;
          visited[start]=1;
          cost[start]=0;
          minimalCost[start]=0;
          int count_vertex=1;
          while(count_vertex!=n){
                    cudaMemcpy(dev_visited,visited,sizeof(int)*107,cudaMemcpyHostToDevice);
                    cudaMemcpy(dev_cost,cost,sizeof(int)*107,cudaMemcpyHostToDevice);
                    cudaMemcpy(dev_minimalCost,minimalCost,sizeof(int)*107,cudaMemcpyHostToDevice);
                    part1<<<1,107>>>(dev_edges,dev_visited,dev_cost,dev_minimalCost,n);
                    part2<<<1,107>>>(dev_visited,dev_cost,dev_minimalCost,n);
                    cudaMemcpy(visited,dev_visited,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    cudaMemcpy(cost,dev_cost,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    cudaMemcpy(minimalCost,dev_minimalCost,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    count_vertex=0;
                    for(int i=1;i<=n;i++){
                              if(!visited[i]){
                                        count_vertex++;
                              }
                    }
          }
          for(int i=1;i<=n;i++){
                    cout<<minimalCost[i]<<" ";
          }
          cout<<endl;

}
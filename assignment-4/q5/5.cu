#include<cuda_runtime.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
using namespace std;
__global__ void bfs_part(int *dev_edges,int *dev_visited,int *dev_distance,int *dev_active){
          int id=threadIdx.x;
          if(dev_active[id]){
                    int s=107*id;
                    for(int i=0;i<107;i++){
                              int value=i;
                              if(dev_edges[s+value]&&!dev_visited[value]){
                                        dev_active[value]=1;
                                        dev_visited[value]=1;
                                        dev_distance[value]=dev_distance[s/107]+1;
                              }
                    }
                    dev_active[id]=0;
                    dev_visited[id]=1;
          }
}
int main(){
          int *edges;
          edges=(int*)malloc(sizeof(int)*107*107);
          memset(edges,0,sizeof(edges));
          int n,m;
          scanf("%d%d",&n,&m);
          for(int i=0;i<m;i++){
                    int a,b;
                    scanf("%d%d",&a,&b);
                    
                    edges[a*107+b]=1;
                    edges[b*107+a]=1;
          } 
          int *dev_edges;
          cudaMalloc((void**)&dev_edges,sizeof(int)*107*107);
          cudaMemcpy(dev_edges,edges,sizeof(int)*107*107,cudaMemcpyHostToDevice);

          
          int visited[107];
          int distance[107];
          int active[107];
          memset(visited,0,sizeof(visited));
          memset(active,0,sizeof(active));
          memset(distance,-1,sizeof(distance));
          int start_vertex=0;
          scanf("%d",&start_vertex);
          distance[start_vertex]=1;
          active[start_vertex]=1;
          
          int count_vertex=1;
          while(count_vertex!=n){
                    int *dev_visited,*dev_distance,*dev_active;
                    cudaMalloc((void**)&dev_visited,sizeof(int)*107);
                    cudaMalloc((void**)&dev_distance,sizeof(int)*107);
                    cudaMalloc((void**)&dev_active,sizeof(int)*107);
                    cudaMemcpy(dev_distance,distance,sizeof(int)*107,cudaMemcpyHostToDevice);
                    cudaMemcpy(dev_visited,visited,sizeof(int)*107,cudaMemcpyHostToDevice);
                    cudaMemcpy(dev_active,active,sizeof(int)*107,cudaMemcpyHostToDevice);
                    bfs_part<<<1,107>>>(dev_edges,dev_visited,dev_distance,dev_active);
                    cudaMemcpy(visited,dev_visited,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    cudaMemcpy(distance,dev_distance,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    cudaMemcpy(active,dev_active,sizeof(int)*107,cudaMemcpyDeviceToHost);
                    count_vertex=0;
                    for(int i=0;i<=n;i++){
                              count_vertex+=visited[i];
                    }
          }
          for(int i=1;i<=n;i++){
                    cout<<distance[i]<<" ";
          }
          cout<<endl;
          cudaDeviceSynchronize();
          return 0;
}
#include<cuda_runtime.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<cmath>
#include<stdlib.h>
using namespace std;
__global__ void sum(int *arr1,int *arr2,int64_t n){
          int id=threadIdx.x;
          if(id<=n){
                    arr1[id]+=arr2[id];
          }
}

int main(){
          int n;
          scanf("%d",&n);
          int *arr1=(int*)malloc(sizeof(int)*(n+1));
          int *arr2=(int*)malloc(sizeof(int)*(n+1));
          for(int i=1;i<=n;i++){
                    arr1[i]=rand()%100;
                    arr2[i]=rand()%100;
          }
          for(int i=1;i<=n;i++){
                    cout<<arr1[i]<<" ";
          }
          cout<<endl;
          for(int i=1;i<=n;i++){
                    cout<<arr2[i]<<" ";
          }
          cout<<endl;
          int *dev_arr1,*dev_arr2;
          cudaMalloc((void**)&dev_arr1,sizeof(int)*(n+1));
          cudaMalloc((void**)&dev_arr2,sizeof(int)*(n+1));
          cudaMemcpy(dev_arr1,arr1,sizeof(int)*(n+1),cudaMemcpyHostToDevice);
          cudaMemcpy(dev_arr2,arr2,sizeof(int)*(n+1),cudaMemcpyHostToDevice);
          sum<<<1,1021>>>(dev_arr1,dev_arr2,n);
          cudaMemcpy(arr1,dev_arr1,sizeof(int)*(n+1),cudaMemcpyDeviceToHost);
          for(int i=1;i<=n;i++){
                    cout<<arr1[i]<<" ";
          }
          cout<<endl;
          cudaDeviceSynchronize();
          
}
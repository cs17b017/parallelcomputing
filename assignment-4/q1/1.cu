#include<cuda_runtime.h>
#include<stdio.h>
#include<iostream>
#include<cmath>
#include<stdlib.h>
using namespace std;
__global__ void sum(int *arr1,int *arr2,int l,int64_t r,int64_t n){
          int id=threadIdx.x;
          if(id+l<=n&&id+l<=r){
                    if(2*(id+l)<=n){
                              arr1[id+l]+=arr2[2*(id+l)];
                    }
                    if(2*(id+l)+1<=n){
                              arr1[id+l]+=arr2[2*(id+l)+1];
                    }
          }
}

int main(){
          int n;
          scanf("%d",&n);
          int *arr=(int*)malloc(sizeof(int)*(n+1));
          for(int i=1;i<=n;i++){
                    arr[i]=i;
          }
          int *dev_arr1,*dev_arr2;
          cudaMalloc((void**)&dev_arr1,sizeof(int)*(n+1));
          cudaMalloc((void**)&dev_arr2,sizeof(int)*(n+1));
          cudaMemcpy(dev_arr1,arr,sizeof(int)*(n+1),cudaMemcpyHostToDevice);
          cudaMemcpy(dev_arr2,arr,sizeof(int)*(n+1),cudaMemcpyHostToDevice);
          int value=log2(n)+1;
          bool flag=0;
          while(value){
                    int l=pow((int)2,value-1),r=2*l-1;
                    if(flag){
                              sum<<<1,1007>>>(dev_arr1,dev_arr2,l,r,n);
                    }
                    else{
                              sum<<<1,1007>>>(dev_arr2,dev_arr1,l,r,n);
                    }
                    if(flag){
                              cudaMemcpy(arr,dev_arr1,sizeof(int)*(n+1),cudaMemcpyDeviceToHost);
                    }
                    else{
                              cudaMemcpy(arr,dev_arr2,sizeof(int)*(n+1),cudaMemcpyDeviceToHost);
                    }
                    flag=!flag;
                    value--;
          }
          if(!flag){
                    cudaMemcpy(arr,dev_arr1,sizeof(int)*(n+1),cudaMemcpyDeviceToHost);
          }
          else{
                    cudaMemcpy(arr,dev_arr2,sizeof(int)*(n+1),cudaMemcpyDeviceToHost);
          }
          cout<<arr[1]<<endl;
          cudaDeviceSynchronize();
          
}